FROM node:10.19.0-alpine3.9

USER root

WORKDIR /usr/src/app

COPY package*.json ./

RUN npm install

COPY . .
EXPOSE 3000
CMD [ "npm", "run", "dev" ]
# docker build --rm -f Dockerfile -t nodebasemongodb:latest .