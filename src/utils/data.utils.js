import crypto from 'crypto';

import mongoose from 'mongoose';

const CODE_MIN_LENGTH = 4;
const CODE_MAX_LENGTH = 24;
const CODE_REGEX = /^[a-zA-Z0-9\\_\\-]+$/;

class DataUtils {
    static isHasValue(value) {
        return value !== null && value !== undefined && !!value.toString().trim();
    }

    static isNumber(value) {
        return !Number.isNaN(parseFloat(value));
    }

    static isValidCode(code) {
        const length = code ? code.length : 0;
        return length >= CODE_MIN_LENGTH && length <= CODE_MAX_LENGTH && CODE_REGEX.test(code);
    }

    /**
     * Method: convert string mongodb id to object mongodbb id
     * @param {String} value string id mongodb
     * @returns {Object} object id mongodb
     */
    static stringToMongoObjectId(value) {
        try {
            if (!value) {
                return null;
            }

            return mongoose.Types.ObjectId(value);
        } catch (error) {
            return null;
        }
    }

    static isMongodbObjectId(value) {
        try {
            return !!mongoose.Types.ObjectId(value);
        } catch (error) {
            return false;
        }
    }

    static randomKey(length) {
        const keyLength = length > 0 ? length : 3;
        return crypto.randomBytes(keyLength).toString('hex').toUpperCase();
    }

    static isEmptyObject(obj) {
        return !!obj && obj.constructor === Object && Object.keys(obj).length === 0;
    }

    static kilobitToMegabyte(kilobit) {
        return Number(kilobit) / 8000;
    }

    static megabyteToGigabyte(megabyte) {
        return Number(megabyte) / 1000;
    }
}

export default DataUtils;
