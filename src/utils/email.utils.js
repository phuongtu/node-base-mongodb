/* eslint-disable no-console */
import nodemailer from 'nodemailer';
import config from 'config';

class EmailUtils {
    static async sendOneMail(receiver, email) {
        const { username, password } = config.email_sender;

        const transport = this.getTransport(username, password);
        const mailOption = this.getMailOption(receiver, email);

        const info = await transport.sendMail(mailOption).catch((error) => {
            console.error(error);
        });

        console.log(info);
    }

    static getTransport(username, password) {
        return nodemailer.createTransport({
            host: 'smtp.gmail.com',
            port: 587,
            secure: false,
            requireTLS: true,
            auth: {
                user: username,
                pass: password
            }
        });
    }

    static getMailOption(receiver, email) {
        const { subject, content, attachments } = email;

        return {
            to: receiver,
            subject,
            text: content,
            attachments
        };
    }
}

export default EmailUtils;
