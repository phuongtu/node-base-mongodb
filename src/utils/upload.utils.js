import fs from 'fs';
import path from 'path';
import util from 'util';

import multer from 'multer';

import RequestUtils from './request.utils';

const storageForPostSingle = multer.diskStorage({
    destination: (req, file, callback) => {
        try {
            const dir = path.join(__dirname, '/../../public/uploads/');
            fs.mkdirSync(dir, { recursive: true });
            callback(null, dir);
        } catch (error) {
            callback(error);
        }
    },
    filename: (req, file, callback) => {
        const userId = RequestUtils.getUserIdFromRequest(req);
        const filetypes = /|jpg|png|jpeg/;

        const mimetype = filetypes.test(file.mimetype);
        const extname = filetypes.test(path.extname(file.originalname));
        if (mimetype || extname) {
            const fileName = userId + file.originalname.replace(/ /g, '');
            callback(null, fileName);
        } else {
            callback('upload file error');
        }
    }
});

export const uploadAvatarStorage = util.promisify(multer({ storage: storageForPostSingle }).single('file'));
