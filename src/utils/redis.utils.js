import redis from 'redis';
import config from 'config';

const redisClient = redis.createClient(config.redis.port, config.redis.host);

class RedisUtils {
    /**
     * function get length of list by key
     * @param {string} key of list
     */
    static _llen(key) {
        return new Promise((resolve, reject) => {
            redisClient.llen(key, (err, length) => (err ? reject(err) : resolve(length)));
        });
    }

    /**
     * function push data on list by key
     * @param {string} key
     * @param {array || string} data
     */
    static _lpush(key, data) {
        return new Promise((resolve, reject) => {
            try {
                redisClient.lpush(key, data);
                resolve();
            } catch (err) {
                reject(err);
            }
        });
    }

    /**
     * function get all data in list by key
     * @param {string} key of list
     */
    static _lrange(key) {
        return new Promise((resolve, reject) => {
            redisClient.lrange(key, 0, -1, (err, data) => (err ? reject(err) : resolve(data)));
        });
    }

    /**
     * function check exists key
     * @param {string} key check exists
     */
    static _exists(key) {
        return new Promise((resolve, reject) => {
            redisClient.exists(key, (err, reply) => {
                if (err) reject(err);
                if (reply === 1) {
                    resolve(true);
                } else {
                    resolve(false);
                }
            });
        });
    }

    /**
     * remove item in list
     * @param {string} key
     * @param {string} data
     */
    static _lrem(key, data) {
        return new Promise((resolve, reject) => {
            try {
                redisClient.lrem(key, 0, data);
                resolve();
            } catch (err) {
                reject(err);
            }
        });
    }
}

export default RedisUtils;
