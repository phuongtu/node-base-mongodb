import mongoose from 'mongoose';
import timestamps from 'mongoose-timestamp';
import bcrypt from 'bcryptjs';
import jwt from 'jsonwebtoken';
import config from 'config';

import { Collection } from '../commons/consts/database.consts';
import { UserRole } from '../commons/consts/user.consts';

const { Schema } = mongoose;
const SALT_ROUND = 10;

const UserSchema = new Schema({
    password: String,
    avatar: String,
    email: {
        type: String,
        index: true
    },
    name: String,
    changePasswordCode: {
        code: {
            type: String,
            default: null
        },
        createdAt: {
            type: Number,
            default: 0
        }
    },
    activeCode: {
        code: {
            type: String,
            default: null
        },
        createdAt: {
            type: Number,
            default: 0
        }
    },
    isActive: Boolean,
    role: {
        type: Number,
        default: UserRole.NORMAL_USER
    }
}, {
    collection: Collection.USER
});

UserSchema.methods.generateAccessToken = function () {
    const { _id, email, role } = this;
    const payload = {
        id: _id,
        email,
        role
    };
    const options = {
        expiresIn: config.token.lifetimes
    };
    const secret = config.token.secret_key;
    // ref: https://www.npmjs.com/package/jsonwebtoken
    return jwt.sign(payload, secret, options);
};

UserSchema.methods.isMatchPasswordSync = async function (plainText) {
    const { password } = this;
    const isMatch = await bcrypt.compareSync(plainText, password);
    return isMatch;
};

UserSchema.pre('save', async function (next) {
    const user = this;
    if (user.isDirectModified('password')) {
        const salt = await bcrypt.genSaltSync(SALT_ROUND);
        const hashedWord = await bcrypt.hashSync(user.password, salt);
        user.password = hashedWord;
    }

    return next();
});

UserSchema.plugin(timestamps);

export default mongoose.model('UserSchema', UserSchema);
