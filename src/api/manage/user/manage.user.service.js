import _ from 'lodash';

import BaseService from '../../../commons/base/service.base';
import ConflictError from '../../../commons/errors/conflict.error';
import NotFoundError from '../../../commons/errors/not_found.error';

import ManagementUserDAO from './manage.user.dao';
import { ManagementUserErrorResponse } from './manage.user.error';

class ManagementUserService extends BaseService {
    constructor() {
        super(new ManagementUserDAO());
    }

    async createNewNormalUser(reqBody) {
        const { email } = reqBody;
        const user = await this.dao.userDAO.getUserByEmail(email);
        if (user) {
            throw new ConflictError(ManagementUserErrorResponse[201]);
        }

        reqBody.email = email.toLowerCase();
        const insertedUser = await this.dao.insertRecord(reqBody);
        return _.pick(insertedUser, ['role', 'email', 'name', '_id']);
    }

    getListUser(sortRules, page, filter) {
        return this.dao.getListUser(sortRules, page, filter);
    }

    countListUser(filter) {
        return this.dao.countListUser(filter);
    }

    async updateInfoUser(userId, reqBody) {
        const user = await this.getRecordById(userId);
        if (!user) {
            throw new NotFoundError(ManagementUserErrorResponse[202]);
        }

        if ('name' in reqBody) {
            user.name = reqBody.name;
        }

        return this.dao.updateRecord(user);
    }
}

export default ManagementUserService;
