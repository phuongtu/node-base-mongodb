import Joi from '@hapi/joi';

import ValidationError from '../../../commons/errors/validation.error';
import { MIN_PASSWORD, REGEX_CHECK_PASSWORD } from '../../../commons/consts/user.consts';

class ManagementUserValidation {
    async _validateCreateNewUser(reqBody) {
        try {
            const schemaValidateAddNewUser = Joi.object({
                name: Joi.string().alphanum().trim().required(),
                email: Joi.string().trim().email().required(),
                password: Joi
                    .string()
                    .pattern(REGEX_CHECK_PASSWORD)
                    .min(MIN_PASSWORD)
                    .trim()
                    .required()
            }).with('email', 'password');

            return await schemaValidateAddNewUser.validateAsync(reqBody);
        } catch (errors) {
            throw new ValidationError(errors);
        }
    }

    async _validateUpdateInfoUser(reqBody) {
        try {
            const schemaValidateUpdateInfoUser = Joi.object({
                name: Joi.string().alphanum().trim()
            });

            return await schemaValidateUpdateInfoUser.validateAsync(reqBody);
        } catch (errors) {
            throw new ValidationError(errors);
        }
    }
}

export default ManagementUserValidation;
