import BaseDAO from '../../../commons/base/dao.base';
import UserSchema from '../../../schemas/user.schema';
import UserDAO from '../../user/user.dao';

class ManagementUserDAO extends BaseDAO {
    constructor() {
        super(UserSchema);
        this.userDAO = new UserDAO();
    }

    getListUser(sortRules, page, filter) {
        let filterData = {};
        if (filter) {
            filterData = {
                $or: [
                    { email: { $regex: filter, $options: 'is' } },
                    { name: { $regex: filter, $options: 'is' } }
                ]
            };
        }
        return this.Schema.find(filterData)
            .select('_id email role name createdAt')
            .sort(sortRules)
            .limit(page.limit)
            .skip(page.skip);
    }

    countListUser(filter) {
        let query = {};
        if (filter) {
            query = {
                $or: [
                    { email: { $regex: filter, $options: 'is' } },
                    { name: { $regex: filter, $options: 'is' } }
                ]
            };
        }
        return this.countRecords(query);
    }
}

export default ManagementUserDAO;
