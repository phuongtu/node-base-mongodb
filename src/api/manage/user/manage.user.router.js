import { Router } from 'express';

import ManagementUserController from './manage.user.controller';

const router = Router();
const managementUserController = new ManagementUserController();

/**
 * @swagger
 * /manage/user:
 *   post:
 *     tags:
 *      - "Management User"
 *     summary: Admin create user
 *     consumes:
 *       - application/json
 *     produces:
 *       - application/json
 *     parameters:
 *       - in: body
 *         name: body
 *         schema:
 *            $ref: "#/parameters/RequestAdminCreateNewUser"
 *         required: true
 *     responses:
 *       '200':
 *         description: A single user object
 *         schema:
 *           $ref: "#/parameters/ResponseManagementUserAddNew"
 *       '404':
 *         description: Email already exists!
 *       '400':
 *         description: Validation error
 */
router.post('/', managementUserController.createNewNomalUser);

/**
 * @swagger
 * /manage/user:
 *   get:
 *     tags:
 *      - "Management User"
 *     summary: Admin get list user
 *     consumes:
 *       - application/json
 *     produces:
 *       - application/json
 *     parameters:
 *       - in: "query"
 *         name: "limit"
 *         description: "limit"
 *         type: "string"
 *       - in: "query"
 *         name: "offset"
 *         description: "offset"
 *         type: "string"
 *       - in: "query"
 *         name: "sort"
 *         description: "sort"
 *         type: "string"
 *       - in: "query"
 *         name: "filter"
 *         description: "filter"
 *         type: "string"
 *     responses:
 *       '200':
 *         description: A single user object
 *         schema:
 *           $ref: "#/parameters/ResponseManagementUserAddNew"
 */
router.get('/', managementUserController.getListUser);

/**
 * @swagger
 * /manage/user/{userId}:
 *   get:
 *     tags:
 *      - "Management User"
 *     summary: Get info user
 *     consumes:
 *       - application/json
 *     produces:
 *       - application/json
 *     parameters:
 *       - in: path
 *         name: userId
 *         required: true
 *         description: "id of user want to get info"
 *     responses:
 *       '200':
 *         description: A single user object
 *         schema:
 *           $ref: "#/parameters/ResponseManagementUserAddNew"
 *       '404':
 *         description: Resource not found
 *       '400':
 *         description: Validation error
 */
router.get('/:userId', managementUserController.getInfoUserById);

/**
 * @swagger
 * /manage/user/{userId}:
 *   patch:
 *     tags:
 *      - "Management User"
 *     summary: Update info user
 *     consumes:
 *       - application/json
 *     produces:
 *       - application/json
 *     parameters:
 *       - in: path
 *         name: userId
 *         required: true
 *         description: "id of user want to update info"
 *       - in: body
 *         name: body
 *         schema:
 *            $ref: "#/parameters/RequestUpdateInfoUser"
 *         required: true
 *     responses:
 *       '204':
 *         description: No content
 *       '404':
 *         description: Email aready exsist!
 *       '400':
 *         description: Validation error
 */
router.patch('/:userId', managementUserController.updateInfoUser);

/**
 * @swagger
 * /manage/user/{userId}:
 *   delete:
 *     tags:
 *      - "Management User"
 *     summary: Delete user
 *     consumes:
 *       - application/json
 *     produces:
 *       - application/json
 *     parameters:
 *       - in: path
 *         name: userId
 *         required: true
 *         description: "id of user want to delete"
 *     responses:
 *       '204':
 *         description: No content
 */
router.delete('/:userId', managementUserController.deleteUser);

export const manageUserRouter = router;
