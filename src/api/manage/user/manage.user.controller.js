import _ from 'lodash';

import BaseController from '../../../commons/base/controller.base';
import PagedData from '../../../commons/responses/page_data.util';
import RequestUtils from '../../../utils/request.utils';

import ManagementUserService from './manage.user.service';
import ManagementUserValidation from './manage.user.validate';

class ManagementUserController extends BaseController {
    constructor() {
        super(new ManagementUserService());
        this.validate = new ManagementUserValidation();
    }

    async createNewNomalUser(req, res) {
        try {
            const reqBody = await this.validate._validateCreateNewUser(req.body);
            const infoUser = await this.service.createNewNormalUser(reqBody);

            return this.response.success(res, infoUser);
        } catch (errors) {
            return this.response.error(res, errors);
        }
    }

    async getListUser(req, res) {
        try {
            const { sort, limit, offset, filter } = req.query;
            const sortRules = RequestUtils.parseSort(sort);
            const page = RequestUtils.getPage(limit, offset);

            const listUser = await this.service.getListUser(sortRules, page, filter);
            const countListUser = await this.service.countListUser(filter);

            const dataFormat = new PagedData(listUser, countListUser, page.skip);
            return this.response.success(res, dataFormat);
        } catch (errors) {
            return this.response.error(res, errors);
        }
    }

    async getInfoUserById(req, res) {
        try {
            const { userId } = req.params;
            const userInfo = await this.service.getRecordById(userId);
            const result = _.pick(userInfo, ['role', 'email', 'name', '_id']);
            return this.response.success(res, result);
        } catch (errors) {
            return this.response.error(res, errors);
        }
    }

    async updateInfoUser(req, res) {
        try {
            const { userId } = req.params;
            const reqBody = await this.validate._validateUpdateInfoUser(req.body);
            await this.service.updateInfoUser(userId, reqBody);
            return this.response.noContent(res);
        } catch (errors) {
            return this.response.error(res, errors);
        }
    }

    async deleteUser(req, res) {
        try {
            const { userId } = req.params;
            await this.service.deleteRecordById(userId);
            return this.response.noContent(res);
        } catch (errors) {
            return this.response.error(res, errors);
        }
    }
}

export default ManagementUserController;
