import BaseController from '../../commons/base/controller.base';
import RequestUtils from '../../utils/request.utils';

import UserService from './user.service';
import UserValidation from './user.validate';


class UserController extends BaseController {
    constructor() {
        super(new UserService());
        this.validate = new UserValidation();
    }

    async createNewNormalUser(req, res) {
        try {
            const reqBody = await this.validate._validateAddNewUser(req.body);
            await this.service.createNewNormalUser(reqBody);

            return this.response.noContent(res);
        } catch (errors) {
            return this.response.error(res, errors);
        }
    }

    async updateInfoUser(req, res) {
        try {
            const reqBody = await this.validate._validateUpdateInfoUser(req.body);
            const userId = RequestUtils.getUserIdFromRequest(req);
            await this.service.updateInfoUser(userId, reqBody);
            return this.response.noContent(res);
        } catch (errors) {
            return this.response.error(res, errors);
        }
    }

    async getInfoUser(req, res) {
        try {
            const userId = RequestUtils.getUserIdFromRequest(req);
            const user = await this.service.getInfoUser(userId);
            return this.response.success(res, user);
        } catch (errors) {
            return this.response.error(res, errors);
        }
    }
}

export default UserController;
