import { Router } from 'express';

import * as authMiddleware from '../../middlewares/auth.middleware';

import UserController from './user.controller';

const router = Router();
const userController = new UserController();

/**
 * @swagger
 * /user:
 *   post:
 *     tags:
 *      - "User"
 *     summary: Register user
 *     description: "Normal user register in the app"
 *     consumes:
 *       - application/json
 *     produces:
 *       - application/json
 *     parameters:
 *       - in: body
 *         name: body
 *         schema:
 *            $ref: "#/parameters/RequestRegister"
 *         required: true
 *     responses:
 *       '204':
 *         description: response no content
 *       '404':
 *         description: Resource not found
 *         schema:
 *              $ref: "#/parameters/RequestRegister"
 *       '400':
 *         description: Validation error
 */
router.post('/', userController.createNewNormalUser);

// add verify token
router.use('/', authMiddleware.verifyAuth);

router.route('/')
    /**
     * @swagger
     * /user:
     *   get:
     *     tags:
     *      - "User"
     *     summary: Get profile of user
     *     consumes:
     *       - application/json
     *     produces:
     *       - application/json
     *     responses:
     *       '200':
     *         description: A single user object
     *         schema:
     *           $ref: "#/parameters/ResponseGetInfoUser"
     */
    .get(userController.getInfoUser)
    /**
     * @swagger
     * /user:
     *   patch:
     *     tags:
     *      - "User"
     *     summary: Update info user
     *     consumes:
     *       - application/json
     *     produces:
     *       - application/json
     *     parameters:
     *       - in: body
     *         name: body
     *         schema:
     *            $ref: "#/parameters/RequestUpdateInfoUser"
     *     responses:
     *       '204':
     *         description: response no content
     *       '400':
     *         description: Validation error
     */
    .patch(userController.updateInfoUser);


export const userRouter = router;
