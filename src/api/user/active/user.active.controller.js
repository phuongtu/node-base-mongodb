// import BaseController from '../../../base/controller.base';

import UserController from '../user.controller';

import ActiveUserService from './user.active.service';
import ActiveUserValidation from './user.active.validate';

class ActiveUserController extends UserController {
    constructor() {
        super(new ActiveUserService());
        this.validate = new ActiveUserValidation();
    }

    async activeNormalUser(req, res) {
        try {
            const reqBody = await this.validate._validateActiveNormalUser(req.body);
            await this.service.activeNormalUser(reqBody);

            return this.response.noContent(res);
        } catch (errors) {
            return this.response.error(res, errors);
        }
    }

    async resendCode(req, res) {
        try {
            const reqBody = await this.validate._validateResendCode(req.body);
            await this.service.resendCode(reqBody);

            return this.response.noContent(res);
        } catch (errors) {
            return this.response.error(res, errors);
        }
    }
}

export default ActiveUserController;
