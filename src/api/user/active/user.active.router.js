import { Router } from 'express';

import ActiveUserController from './user.active.controller';

const router = Router();
const activeUserController = new ActiveUserController();

/**
 * @swagger
 * /user/active:
 *   post:
 *     tags:
 *      - "User"
 *     summary: Normal user active account when register
 *     consumes:
 *       - application/json
 *     produces:
 *       - application/json
 *     parameters:
 *       - in: body
 *         name: body
 *         schema:
 *            $ref: "#/parameters/RequestActiveUser"
 *         required: true
 *     responses:
 *       '204':
 *         description: response no content
 *       '404':
 *         description: User not found!
 *       '400':
 *         description: Code not match, Code has expired
 */
router.post('/', activeUserController.activeNormalUser);

/**
 * @swagger
 * /user/active/resend:
 *   post:
 *     tags:
 *      - "User"
 *     summary: Normal user resend active code
 *     consumes:
 *       - application/json
 *     produces:
 *       - application/json
 *     parameters:
 *       - in: body
 *         name: body
 *         schema:
 *            $ref: "#/parameters/RequestResendCodeAndForgotPassword"
 *         required: true
 *     responses:
 *       '204':
 *         description: response no content
 *       '404':
 *         description: User not found!
 *       '409':
 *         description: User has active
 */
router.post('/resend', activeUserController.resendCode);

export const userActiveRouter = router;
