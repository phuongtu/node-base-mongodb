import Joi from '@hapi/joi';

import ValidationError from '../../../commons/errors/validation.error';

class ActiveUserValidation {
    async _validateActiveNormalUser(reqBody) {
        try {
            const schemaValidateActiveNormalUser = Joi.object({
                email: Joi.string().trim().email().required(),
                activeCode: Joi.string().trim().required()
            });

            return await schemaValidateActiveNormalUser.validateAsync(reqBody);
        } catch (errors) {
            throw new ValidationError(errors);
        }
    }

    async _validateResendCode(reqBody) {
        try {
            const schemaValidateResendCode = Joi.object({
                email: Joi.string().trim().email().required()
            });

            return await schemaValidateResendCode.validateAsync(reqBody);
        } catch (errors) {
            throw new ValidationError(errors);
        }
    }
}

export default ActiveUserValidation;
