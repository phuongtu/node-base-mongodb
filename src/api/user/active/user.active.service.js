import config from 'config';

import BaseService from '../../../commons/base/service.base';
import UserDAO from '../user.dao';
import NotFoundError from '../../../commons/errors/not_found.error';
import ValidationError from '../../../commons/errors/validation.error';
import ConflictError from '../../../commons/errors/conflict.error';
import DateTimeUtils from '../../../utils/date_time.utils';
import UserService from '../user.service';
import DataUtils from '../../../utils/data.utils';
import { UserErrorResponse } from '../user.error';

class ActiveUserService extends BaseService {
    constructor() {
        super(new UserDAO());
        this.userService = new UserService();
    }

    async activeNormalUser(reqBody) {
        const { email, activeCode } = reqBody;
        const user = await this.dao.getUserByEmail(email);

        if (!user) {
            throw new NotFoundError(UserErrorResponse[204]);
        }

        if (user.activeCode.code !== activeCode) {
            const error = UserErrorResponse[205];
            error.label = 'activeCode';
            throw new ValidationError(error);
        }

        const codeCreatedAt = user.activeCode.createdAt;
        const currentMsTime = DateTimeUtils.getCurrentMsTime();
        const codeLifeTimes = config.code_life_times;

        if (codeCreatedAt + codeLifeTimes < currentMsTime) {
            const error = UserErrorResponse[206];
            error.label = 'activeCode';
            throw new ValidationError(error);
        }

        user.isActive = true;
        user.activeCode = {
            code: null,
            createdAt: 0
        };

        return this.dao.updateRecord(user);
    }

    async resendCode(reqBody) {
        const { email } = reqBody;
        const user = await this.dao.getUserByEmail(email);

        if (!user) {
            throw new NotFoundError(UserErrorResponse[204]);
        }

        if (user.isActive) {
            throw new ConflictError(UserErrorResponse[207]);
        }

        const activeCode = DataUtils.randomKey(3);
        const currentMsTime = DateTimeUtils.getCurrentMsTime();

        user.activeCode = {
            code: activeCode,
            createdAt: currentMsTime
        };

        setTimeout(this.userService.sendNewUserEmail, 1000, user, activeCode);

        return this.dao.updateRecord(user);
    }
}

export default ActiveUserService;
