import BaseDAO from '../../commons/base/dao.base';
import UserSchema from '../../schemas/user.schema';

class UserDAO extends BaseDAO {
    constructor() {
        super(UserSchema);
    }

    /**
     * Method: get info user by email
     * @param {String} email want to get info
     */
    async getUserByEmail(email) {
        const conditions = { email: email.toLowerCase() };
        return this.Schema.findOne(conditions);
    }

    /**
     * Method get info user by id
     * @param {*} id of user want to get info
     */
    getUserInfoById(id) {
        return this.Schema.findOne({ _id: id }).select('email role name avatar');
    }
}

export default UserDAO;
