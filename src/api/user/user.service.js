import BaseService from '../../commons/base/service.base';
import ConflictError from '../../commons/errors/conflict.error';
import DataUtils from '../../utils/data.utils';
import DateTimeUtils from '../../utils/date_time.utils';
import EmailUtils from '../../utils/email.utils';

import { UserErrorResponse } from './user.error';
import UserDAO from './user.dao';

class UserService extends BaseService {
    constructor() {
        super(new UserDAO());
    }

    /**
     * Method: create new normal user
     * @param {*} reqBody request body info user want to create
     */
    async createNewNormalUser(reqBody) {
        const { email } = reqBody;
        const user = await this.dao.getUserByEmail(email);
        if (user) {
            throw new ConflictError(UserErrorResponse[203]);
        }
        const activeCode = DataUtils.randomKey(3);
        const currentMsTime = DateTimeUtils.getCurrentMsTime();

        const userData = Object.assign(reqBody, { activeCode: { code: activeCode, createdAt: currentMsTime } });
        userData.email = email.toLowerCase();
        const insertedUser = await this.dao.insertRecord(userData);

        setTimeout(this.sendNewUserEmail, 1000, userData, activeCode);

        return insertedUser;
    }

    /**
     * Method: send active co to email of user when create new user of resend active code
     * @param {*} user info want to send active code to email
     * @param {*} activeCode active code of user
     */
    sendNewUserEmail(user, activeCode) {
        const { email, name } = user;
        const subject = 'Active user';
        const content = `Hi ${name}, your active code is ${activeCode}`;
        const emailToSend = { subject, content };
        // send mail to user
        EmailUtils.sendOneMail(email, emailToSend);
    }

    getInfoUser(userId) {
        return this.dao.getUserInfoById(userId);
    }

    updateInfoUser(userId, dataUpdate) {
        return this.dao.updateRecordById(userId, dataUpdate);
    }
}

export default UserService;
