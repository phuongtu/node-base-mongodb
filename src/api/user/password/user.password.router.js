import { Router } from 'express';

import * as authMiddleware from '../../../middlewares/auth.middleware';

import PasswordUserController from './user.password.controller';


const router = Router();
const passwordUserController = new PasswordUserController();

/**
 * @swagger
 * /user/password/forgot:
 *   post:
 *     tags:
 *      - "User"
 *     summary: Nomal user forgot password
 *     consumes:
 *       - application/json
 *     produces:
 *       - application/json
 *     parameters:
 *       - in: body
 *         name: body
 *         description: "Email of user want to forgot password"
 *         schema:
 *            $ref: "#/parameters/RequestResendCodeAndForgotPassword"
 *         required: true
 *     responses:
 *       '204':
 *         description: response no content
 *       '404':
 *         description: User not found!
 *       '400':
 *         description: Validation error
 */
router.post('/forgot', passwordUserController.forgotPassword);

/**
 * @swagger
 * /user/password/update:
 *   post:
 *     tags:
 *      - "User"
 *     summary: Nomal user update password by code
 *     consumes:
 *       - application/json
 *     produces:
 *       - application/json
 *     parameters:
 *       - in: body
 *         name: body
 *         schema:
 *            $ref: "#/parameters/RequestUpdatePasswordByCode"
 *         required: true
 *     responses:
 *       '204':
 *         description: response no content
 *       '404':
 *         description: User not found!
 *       '400':
 *         description: Validation error, Code not match, Code has expired
 */
router.post('/update', passwordUserController.updatePasswordByCode);

// add verify token
router.use('/', authMiddleware.verifyAuth);

/**
 * @swagger
 * /user/password/change:
 *   post:
 *     tags:
 *      - "User"
 *     summary: User change password
 *     consumes:
 *       - application/json
 *     produces:
 *       - application/json
 *     parameters:
 *       - in: body
 *         name: body
 *         schema:
 *            $ref: "#/parameters/RequestUserChangePassword"
 *         required: true
 *     responses:
 *       '204':
 *         description: response no content
 *       '404':
 *         description: User not found!
 *       '400':
 *         description: Validation error, Password does not match
 */
router.post('/change', passwordUserController.changePassword);

export const userPasswordRouter = router;
