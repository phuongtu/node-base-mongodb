import Joi from '@hapi/joi';

import ValidationError from '../../../commons/errors/validation.error';
import { MIN_PASSWORD, REGEX_CHECK_PASSWORD } from '../../../commons/consts/user.consts';

class PasswordUserValidation {
    async _validateForgotPassword(reqBody) {
        try {
            const schemaValidateForGotPassword = Joi.object({
                email: Joi.string().trim().email().required()
            });

            return await schemaValidateForGotPassword.validateAsync(reqBody);
        } catch (errors) {
            throw new ValidationError(errors);
        }
    }

    async _validateUpdatePasswordByCode(reqBody) {
        try {
            const schemaValidateUpdatePasswordByCode = Joi.object({
                email: Joi.string().trim().email().required(),
                code: Joi.string().trim().required(),
                password: Joi
                    .string()
                    .pattern(REGEX_CHECK_PASSWORD)
                    .min(MIN_PASSWORD)
                    .trim()
                    .required()
            });

            return await schemaValidateUpdatePasswordByCode.validateAsync(reqBody);
        } catch (errors) {
            throw new ValidationError(errors);
        }
    }

    async _validateChangePassword(reqBody) {
        try {
            const schemaValidateChangePassword = Joi.object({
                newPassword: Joi
                    .string()
                    .pattern(REGEX_CHECK_PASSWORD)
                    .min(MIN_PASSWORD)
                    .trim()
                    .required(),
                currentPassword: Joi
                    .string()
                    .pattern(/^[a-zA-Z0-9]{3,30}$/)
                    .min(MIN_PASSWORD)
                    .trim()
                    .required()
            });

            return await schemaValidateChangePassword.validateAsync(reqBody);
        } catch (errors) {
            throw new ValidationError(errors);
        }
    }

    async _validateResendUpdatePasswordCode(reqBody) {
        try {
            const schemaValidateResendUpdatePasswordCode = Joi.object({
                email: Joi.string().trim().email().required()
            });

            return await schemaValidateResendUpdatePasswordCode.validateAsync(reqBody);
        } catch (errors) {
            throw new ValidationError(errors);
        }
    }
}

export default PasswordUserValidation;
