import BaseController from '../../../commons/base/controller.base';

import PasswordUserService from './user.password.service';
import PasswordUserValidation from './user.password.validate';

class PasswordUserController extends BaseController {
    constructor() {
        super(new PasswordUserService());
        this.validate = new PasswordUserValidation();
    }

    async forgotPassword(req, res) {
        try {
            const reqBody = await this.validate._validateForgotPassword(req.body);
            await this.service.forgotPassword(reqBody);

            return this.response.noContent(res);
        } catch (errors) {
            return this.response.error(res, errors);
        }
    }

    async updatePasswordByCode(req, res) {
        try {
            const reqBody = await this.validate._validateUpdatePasswordByCode(req.body);
            await this.service.updatePasswordByCode(reqBody);

            return this.response.noContent(res);
        } catch (errors) {
            return this.response.error(res, errors);
        }
    }

    async changePassword(req, res) {
        try {
            const reqBody = await this.validate._validateChangePassword(req.body);
            await this.service.changePassword(reqBody);

            return this.response.noContent(res);
        } catch (errors) {
            return this.response.error(res, errors);
        }
    }
}

export default PasswordUserController;
