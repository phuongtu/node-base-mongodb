import config from 'config';

import BaseService from '../../../commons/base/service.base';
import UserDAO from '../user.dao';
import NotFoundError from '../../../commons/errors/not_found.error';
import ValidationError from '../../../commons/errors/validation.error';
import DateTimeUtils from '../../../utils/date_time.utils';
import UserService from '../user.service';
import DataUtils from '../../../utils/data.utils';
import EmailUtils from '../../../utils/email.utils';
import { UserErrorResponse } from '../user.error';

class PasswordUserService extends BaseService {
    constructor() {
        super(new UserDAO());
        this.userService = new UserService();
    }

    async forgotPassword(reqBody) {
        const { email } = reqBody;
        const user = await this.dao.getUserByEmail(email);
        if (!user) {
            throw new NotFoundError(UserErrorResponse[204]);
        }

        const changePasswordCode = DataUtils.randomKey(3);
        const currentMsTime = DateTimeUtils.getCurrentMsTime();

        user.changePasswordCode = {
            code: changePasswordCode,
            createdAt: currentMsTime
        };

        setTimeout(this.sendForgotPasswordEmail, 1000, user, changePasswordCode);

        return this.dao.updateRecord(user);
    }

    async updatePasswordByCode(reqBody) {
        const { email, changePasswordCode, password } = reqBody;
        const user = await this.dao.getUserByEmail(email);
        if (!user) {
            throw new NotFoundError(UserErrorResponse[204]);
        }

        if (user.changePasswordCode.code !== changePasswordCode) {
            const error = UserErrorResponse[205];
            error.label = 'changePasswordCode';
            throw new ValidationError(error);
        }

        const codeCreatedAt = user.changePasswordCode.createdAt;
        const currentMsTime = DateTimeUtils.getCurrentMsTime();

        const codeLifeTimes = config.code_life_times;

        if (codeCreatedAt + codeLifeTimes < currentMsTime) {
            const error = UserErrorResponse[206];
            error.label = 'changePasswordCode';
            throw new ValidationError(error);
        }

        user.password = password;
        user.changePasswordCode = {
            code: null,
            createdAt: 0
        };

        return this.dao.updateRecord(user);
    }

    async changePassword(reqBody) {
        const { email, currentPassword, newPassword } = reqBody;

        const user = await this.dao.getUserByEmail(email);
        if (!user) {
            throw new NotFoundError(UserErrorResponse[204]);
        }

        const isMatchPassword = await user.isMatchPasswordSync(currentPassword);

        if (!isMatchPassword) {
            const error = UserErrorResponse[208];
            error.label = 'currentPassword';
            throw new ValidationError(error);
        }

        user.password = newPassword;

        return this.dao.updateRecord(user);
    }

    /**
     * Method: send code forgot password to user
     * @param {*} user info user want to forgot password
     * @param {*} changePasswordCode code change password
     */
    sendForgotPasswordEmail(user, changePasswordCode) {
        const { email, name } = user;
        const subject = 'Update password';
        const content = `Hi ${name}, use ${changePasswordCode} to update your password`;
        const emailToSend = {
            subject,
            content
        };
        // send email to user
        EmailUtils.sendOneMail(email, emailToSend);
    }
}

export default PasswordUserService;
