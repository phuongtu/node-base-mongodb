import Joi from '@hapi/joi';

import ValidationError from '../../commons/errors/validation.error';

class AuthenticateValidation {
    async _validateAuthenticateNormalUser(requestBody) {
        try {
            const schemaValidateAuthenticateNomalUser = Joi.object({
                email: Joi.string().trim().email().required(),
                password: Joi.string().regex(/^[a-zA-Z0-9]{3,30}$/).required()
            }).with('email', 'password');

            return await schemaValidateAuthenticateNomalUser.validateAsync(requestBody);
        } catch (errors) {
            throw new ValidationError(errors);
        }
    }
}

export default AuthenticateValidation;
