import { Router } from 'express';

import AuthenticateController from './auth.controller';

const router = Router();
const authenticateController = new AuthenticateController();

/**
 * @swagger
 * /auth:
 *   post:
 *     tags:
 *      - "Authenticate"
 *     summary: Nomal user authenticate
 *     consumes:
 *       - application/json
 *     produces:
 *       - application/json
 *     parameters:
 *       - in: body
 *         name: body
 *         description: "User want to access to the app"
 *         schema:
 *            $ref: "#/parameters/RequestNormalAuth"
 *         required: true
 *     responses:
 *       '200':
 *         description: A single user object
 *         schema:
 *           $ref: "#/parameters/ResponseNormalAuth"
 *       '404':
 *         description: Resource not found
 *       '400':
 *         description: Validation error
 */
router.post('/', authenticateController.authenticateNormalUser);

export const authRouter = router;
