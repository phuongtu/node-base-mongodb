import BaseService from '../../commons/base/service.base';
import NotFoundError from '../../commons/errors/not_found.error';
import UserDAO from '../user/user.dao';

import { AutheticateErrorResponse } from './auth.error';

class AuthenticateService extends BaseService {
    constructor() {
        super(new UserDAO());
    }

    async authenticateNormalUser(reqBody) {
        const { email, password } = reqBody;

        const user = await this.dao.getUserByEmail(email);
        if (!user) {
            throw new NotFoundError(AutheticateErrorResponse.email_or_pass_not_exists);
        }

        const isMatchPassword = await user.isMatchPasswordSync(password);
        if (!isMatchPassword) {
            throw new NotFoundError(AutheticateErrorResponse.email_or_pass_not_exists);
        }

        if (!user.isActive) {
            throw new NotFoundError(AutheticateErrorResponse.user_not_active);
        }

        return this.getResult(user);
    }

    async getResult(user) {
        const {
            email, name, role, _id, avatar
        } = user;
        const accessToken = user.generateAccessToken();
        const result = {
            _id,
            email,
            name,
            avatar,
            role,
            accessToken
        };

        return result;
    }
}

export default AuthenticateService;
