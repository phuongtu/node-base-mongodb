import BaseController from '../../commons/base/controller.base';

import AuthenticateService from './auth.service';
import AuthenticateValidation from './auth.validate';

class AuthenticateController extends BaseController {
    constructor() {
        super(new AuthenticateService());
        this.validate = new AuthenticateValidation();
    }

    async authenticateNormalUser(req, res) {
        try {
            const reqBody = await this.validate._validateAuthenticateNormalUser(req.body);
            const data = await this.service.authenticateNormalUser(reqBody);

            return this.response.success(res, data);
        } catch (error) {
            return this.response.error(res, error);
        }
    }
}

export default AuthenticateController;
