/* eslint-disable no-unused-vars */
/* eslint-disable consistent-return */

import chai from 'chai';
import chaiHttp from 'chai-http';
import chaiAsPromised from 'chai-as-promised';

import UserSchema from '../../../schemas/user.schema';
import app from '../../../connects/mock.app';

chai.use(chaiAsPromised);
chai.use(chaiHttp);
chai.should();
const { assert } = chai;

describe('Test Authenticate normal user', () => {
    const email = 'test@gmail.com';
    const password = '123456';
    const emailNotActive = 'testnotactive@gmail.com';

    before(() => {
        const userTestSchema = new UserSchema({
            email,
            password,
            name: 'asd',
            isActive: true
        });
        const userTest1Schema = new UserSchema({
            email: emailNotActive,
            password,
            name: 'asd',
            isActive: false
        });
        return Promise.all([
            userTestSchema.save(),
            userTest1Schema.save()
        ]);
    });

    after(() => new Promise((resolve) => resolve(UserSchema.deleteMany({}))));

    describe('#POST /api/v1/auth', () => {
        it('Authenticate success', () => (
            chai.request(app)
                .post('/api/v1/auth')
                .send({ email, password })
                .then((res) => {
                    res.should.have.status(200);
                    res.body.should.be.a('object');
                    res.body.should.have.property('code');
                    res.body.code.should.equal(200);
                    res.body.should.have.property('data');
                    res.body.data.should.be.a('object');
                    res.body.data.should.have.property('_id');
                    res.body.data.should.have.property('accessToken');
                    res.body.data.should.have.property('email');
                    res.body.data.should.have.property('role');
                })
        ));

        it('Authentication failed with password wrong', () => (
            chai.request(app)
                .post('/api/v1/auth')
                .send({ email, password: 'phuongt' })
                .then((res) => {
                    const errorData = JSON.parse(res.error.text);
                    res.should.have.status(404);
                    errorData.should.be.a('object');
                    errorData.should.have.property('code');
                    errorData.should.have.property('message');
                    errorData.should.have.property('error');
                    errorData.code.should.equal(201);
                    errorData.message.should.equal("Email or password don't not exists!");
                    errorData.error.should.equal('Resource not found');
                })
        ));

        it('Authentication failed with email format wrong', () => {
            const errorResponse = {
                code: 400,
                error: 'Validation failed',
                label: 'email',
                message: '"email" must be a valid email'
            };
            return chai.request(app)
                .post('/api/v1/auth')
                .send({ email: 'lephuongtu271094mail.co', password })
                .then((res) => {
                    const errorData = JSON.parse(res.error.text);
                    res.should.have.status(400);
                    errorData.should.be.a('object');
                    assert.deepStrictEqual(errorData, errorResponse);
                });
        });

        it('Authentication failed with email not exists', () => (
            chai.request(app)
                .post('/api/v1/auth')
                .send({ email: 'lephuongtu27101994@gmail.com', password })
                .then((res) => {
                    const errorData = JSON.parse(res.error.text);
                    res.should.have.status(404);
                    errorData.should.be.a('object');
                    errorData.should.have.property('code');
                    errorData.should.have.property('message');
                    errorData.should.have.property('error');
                    errorData.code.should.equal(201);
                    errorData.message.should.equal("Email or password don't not exists!");
                    errorData.error.should.equal('Resource not found');
                })
        ));

        it('Authentication failed with email not active', () => (
            chai.request(app)
                .post('/api/v1/auth')
                .send({ email: emailNotActive, password })
                .then((res) => {
                    const errorData = JSON.parse(res.error.text);
                    res.should.have.status(404);
                    errorData.should.be.a('object');
                    errorData.should.have.property('code');
                    errorData.should.have.property('message');
                    errorData.should.have.property('error');
                    errorData.code.should.equal(202);
                    errorData.message.should.equal('User has not activated the account yet');
                    errorData.error.should.equal('Resource not found');
                })
        ));
    });
});
