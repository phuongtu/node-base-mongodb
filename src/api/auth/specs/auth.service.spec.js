/* eslint-disable no-unused-vars */
/* eslint-disable consistent-return */

import chai from 'chai';
import chaiAsPromised from 'chai-as-promised';

import UserSchema from '../../../schemas/user.schema';
import AuthenticateService from '../auth.controller';

const authenticateService = new AuthenticateService();
chai.use(chaiAsPromised);
chai.should();

describe('Test service authenticate normal user: authenticateNormalUser(reqBody)', () => {
    UserSchema.deleteMany({});
    const email = 'lephuongtu271094@gmail.com';
    const password = 'phuongtu';
    const reqBody = {
        email,
        password
    };
    const emailNotActive = 'lephuongtu27101094@gmail.com';

    before(() => {
        const userTestSchema = new UserSchema({
            email,
            password,
            name: 'asd',
            isActive: true
        });
        const userTest1Schema = new UserSchema({
            email: emailNotActive,
            password,
            name: 'asd',
            isActive: false
        });
        return Promise.all([
            userTestSchema.save(),
            userTest1Schema.save()
        ]);
    });

    after(() => new Promise((resolve) => resolve(UserSchema.deleteMany({}))));

    it('email and password pass', () => (
        authenticateService.authenticateNormalUser(reqBody)
            .then((user) => {
                user.should.be.a('object');
                user.should.have.property('_id');
                user.should.have.property('accessToken');
                user.should.have.property('email');
                user.should.have.property('role');
                user.should.have.property('avatar');
            })
    ));

    it('failed with password wrong', () => {
        const data = Object.assign({}, reqBody);
        data.password = 'phuongt';
        return authenticateService.authenticateNormalUser(data)
            .catch((err) => {
                err.should.be.a('object');
                err.should.have.property('errors');
                err.should.have.property('code');
                err.code.should.equal(404);
                err.errors.should.have.property('code');
                err.errors.should.have.property('message');
                err.errors.should.have.property('error');
                err.errors.code.should.equal(201);
                err.errors.message.should.equal('Email or pasword don\'t not exists!');
                err.errors.error.should.equal('Resource not found');
            });
    });

    it('failed with email not exists', () => {
        const data = Object.assign({}, reqBody);
        data.email = 'lephuongtu27101994mail.co';
        return authenticateService.authenticateNormalUser(data)
            .catch((err) => {
                err.should.be.a('object');
                err.should.have.property('errors');
                err.should.have.property('code');
                err.code.should.equal(404);
                err.errors.should.have.property('code');
                err.errors.should.have.property('message');
                err.errors.should.have.property('error');
                err.errors.code.should.equal(201);
                err.errors.message.should.equal('Email or pasword don\'t not exists!');
                err.errors.error.should.equal('Resource not found');
            });
    });

    it('failed with email not active', () => {
        const data = Object.assign({}, reqBody);
        data.email = emailNotActive;
        return authenticateService.authenticateNormalUser(data)
            .catch((err) => {
                err.should.be.a('object');
                err.should.have.property('errors');
                err.should.have.property('code');
                err.code.should.equal(404);
                err.errors.should.have.property('code');
                err.errors.should.have.property('message');
                err.errors.should.have.property('error');
                err.errors.code.should.equal(202);
                err.errors.message.should.equal('User has not activated the account yet');
                err.errors.error.should.equal('Resource not found');
            });
    });
});
