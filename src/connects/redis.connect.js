import redis from 'redis';
import config from 'config';

const client = redis.createClient(config.redis.port, config.redis.host);
const connectRedis = () => (
    new Promise((resolve, reject) => {
        client.on('connect', () => {
            resolve('Redis client connected');
        });

        client.on('error', (err) => {
            reject(err);
        });
    })
);

export default connectRedis;
