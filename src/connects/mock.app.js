/* eslint-disable no-unused-vars */
/* eslint-disable no-console */
import path from 'path';

import express from 'express';
import bodyParser from 'body-parser';
import mongoose from 'mongoose';
import i18n from 'i18n';

import * as routerUtil from '../utils/router.utils';
import { I18N_CONFIG } from '../../config/i18n.config';

const app = express();
const mongodbOptions = {
    useNewUrlParser: true,
    useUnifiedTopology: true,
    reconnectTries: Number.MAX_VALUE,
    reconnectInterval: 500,
    poolSize: 20,
    useCreateIndex: true
};

mongoose.connect('mongodb://localhost:27017/node_base_test?authSource=node_base_test', mongodbOptions);
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));

i18n.configure(I18N_CONFIG);
app.use(i18n.init);

app.use('/api/v1', routerUtil.loadRouters(path.join(__dirname, '/../api')));

const server = app.listen(9090);

export default server;
