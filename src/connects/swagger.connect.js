/**
 * Copyright (C) 2019 TuLP - All Rights Reserved
 *
 */

/**
 * Connect swagger api
 * ref https://medium.com/@akilaaroshana/design-apis-easier-than-ever-with-swagger-2-0-60a2ba696d7d
 * ref https://editor.swagger.io/
 * ref https://swagger.io/docs/specification/2-0/basic-structure/
 */

import swaggerJSDoc from 'swagger-jsdoc';
import swaggerUi from 'swagger-ui-express';

import { options } from '../../config/swagger';

const swaggerSpec = swaggerJSDoc(options);

const customCss = {
    customCss: '.swagger-ui .topbar { display: none };'
};

export default (app) => {
    app.use('/api-docs', swaggerUi.serve, swaggerUi.setup(swaggerSpec, customCss));
};
