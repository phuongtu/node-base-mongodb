/* eslint-disable no-console */
import path from 'path';

import express from 'express';
import 'express-async-errors';
import bodyParser from 'body-parser';
import cors from 'cors';
import helmet from 'helmet';
import i18n from 'i18n';
import config from 'config';

import * as routerUtil from '../utils/router.utils';
import { ResponseCode } from '../commons/consts/response.consts';
import { I18N_CONFIG } from '../../config/i18n.config';
import Response from '../commons/responses';
import loger from '../utils/loger';

import swagger from './swagger.connect';
import connectMongodb from './mongodb.connect';
import connectRedis from './redis.connect';
import { ConnectsErrorResponse } from './connects.error.response';

const response = new Response();
// config i18n
i18n.configure(I18N_CONFIG);

export default async (app) => {
    app.use(cors('*'));
    app.use(helmet());
    app.use(express.json());
    app.use(express.urlencoded({
        extended: true
    }));
    app.use(bodyParser.json());
    app.use(i18n.init);
    app.use('/api/v1/', express.static(path.join(__dirname, '../../public')));
    app.use('/coverage', express.static(path.join(__dirname, '../../coverage')));
    app.use('/report', express.static(path.join(__dirname, '../../report.html')));

    await Promise.all([
        // connect mongodb database
        connectMongodb,
        // connect redis database
        connectRedis,
        // run cron job
        // cronJob()
        // connect swagger
        swagger(app),
        // run logger
        loger(app),
        // use router api
        app.use(`/api/${config.api_version}`, routerUtil.loadRouters(path.join(__dirname, '/../api')))
    ]);

    /* eslint-disable no-unused-vars */
    // handle app error
    app.use((error, req, res, next) => {
        response.error(res, error);
    });

    // handle end point not found
    app.use((req, res, next) => {
        const errors = JSON.parse(JSON.stringify(ConnectsErrorResponse.connect_error));
        console.error(errors);
        errors.error = res.__(errors.error);
        res.status(ResponseCode.NOT_FOUND).json(errors);
    });
};
