import mongoose from 'mongoose';
import config from 'config';

const mongodbOptions = {
    user: config.db.username,
    pass: config.db.password,
    useNewUrlParser: true,
    useUnifiedTopology: true,
    poolSize: 20
};

const mongoConnection = `mongodb://${config.db.host}/${config.db.name}?authSource=${config.db.name}`;
mongoose.set('useCreateIndex', true);
mongoose.connect(mongoConnection, mongodbOptions);

export default mongoose;
