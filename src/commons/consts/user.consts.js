export const UserAuthenticateType = {
    NORMAL_USER: 1,
    GG_USER: 2,
    FB_USER: 3
};

export const UserRole = {
    NORMAL_USER: 0,
    ADMIN: 1
};

export const MIN_PASSWORD = 6;
export const REGEX_CHECK_PASSWORD = /^[a-zA-Z0-9]{3,30}$/;
