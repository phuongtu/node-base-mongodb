export const AuthenticationConsts = {
    HEADER_AUTHENTICATION_KEY: 'x-access-token',
    TOKEN_EXPIRED_ERROR: 'TokenExpiredError',
    JSON_WEB_TOKEN_ERROR: 'JsonWebTokenError'
};
