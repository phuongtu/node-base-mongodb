import DataUtils from '../../utils/data.utils';
import NotFoundError from '../errors/not_found.error';

import { BASE_SERVICE_ERROR } from './base.error';

class BaseService {
    constructor(dao) {
        this.dao = dao;
    }

    async deleteRecordById(id) {
        await this.dao.deleteRecordById(id);
    }

    async getRecordById(id) {
        const idObject = DataUtils.stringToMongoObjectId(id);

        if (!idObject) {
            throw new NotFoundError(BASE_SERVICE_ERROR.NOT_FOUND);
        }

        const record = await this.dao.getRecordById(idObject);

        if (!record) {
            throw new NotFoundError(BASE_SERVICE_ERROR.NOT_FOUND);
        }

        return record;
    }

    async getRawRecordById(id) {
        if (!DataUtils.isMongodbObjectId(id)) {
            throw new NotFoundError(BASE_SERVICE_ERROR.NOT_FOUND);
        }

        const record = await this.dao.getRawRecordById(id);

        if (!record) {
            throw new NotFoundError(BASE_SERVICE_ERROR.NOT_FOUND);
        }

        return record;
    }

    async updateRecord(record) {
        await this.dao.updateRecord(record);
    }

    async getRawRecordsByIds(ids) {
        const validIds = ids.map((id) => DataUtils.stringToMongoObjectId(id)).filter((id) => id);
        const records = await this.dao.getRawRecordsByIds(validIds);

        return records;
    }
}

export default BaseService;
