# Setup Server
# Required

# Ubuntu version 16.04 or latest

# Nodejs 10.x

# Mongodb 3.2 or latest

    sudo su
    sudo apt-get update

## Setup mongodb:

### Import the Public Key used by the Ubuntu Package Manager

    sudo apt-key adv --keyserver hkp://keyserver.ubuntu.com:80 --recv 9DA31620334BD75D9DCB49F368818C72E52529D4

### Create a file list for mongoDB to fetch the current repository

-   unbuntu 18.04
    echo "deb [ arch=amd64 ] https://repo.mongodb.org/apt/ubuntu bionic/mongodb-org/4.0 multiverse" | sudo tee /etc/apt/sources.list.d/mongodb.list
-   unbuntu 16.04
    echo "deb [ arch=amd64,arm64 ] https://repo.mongodb.org/apt/ubuntu xenial/mongodb-org/4.0 multiverse" | sudo tee /etc/apt/sources.list.d/mongodb.list

### Update the Ubuntu Packages

    apt update

## Install MongoDB

    sudo apt install -y mongodb-org
    systemctl enable mongod //startup vps

## Setup nginx

    sudo apt install -y nginx

    systemctl enable nginx

## Setup nodejs

    sudo apt install curl

    curl -sL https://deb.nodesource.com/setup_10.x | sudo bash -

    sudo apt-get install -y nodejs

## Install pm2

    npm install pm2 -g

## Create folder for domains

    sudo mkdir -p /home/node_base
    sudo chown -R www-data:www-data /home/node_base
    sudo mkdir -p /home/vmodev/cibes-web
    sudo chown -R www-data:www-data /home/vmodev/cibes-web

## Clone source code

    cd /home/node_base/
    git init
    git remote add origin git-url-here
    git pull
    git checkout develop

# Install & start api

    make a file .env at /home/node_base with content:
    sudo nano .env
        PORT = 4000
        JWT_SECRET = node_base@2019
        JWT_SECRET_EXP= 168h
        NODE_ENV = development
        MONGOOSE_URI = mongodb://localhost:27017/node_base
        EMAIL_USERNAME = node_base@gmail.com
        ClientId = 946876502571-lc682606fng08nmbobovpsqkd8rv2rgj.apps.googleusercontent.com
        ClientSecret = OYxZOnfcfRFUfbN7ePbhiV6v
        GMAIL_RefreshToken = 1/C8VuCagv76GlKIuqgRElHbUjxHH1RijuDj5yJ_lSldM

    sudo npm i

###   enable pm2 script boot vps server

    sudo pm2 startup systemd

###   start pm2 server api

    sudo pm2 start --name "node_base" npm -- start -i max

    pm2 save

    # Build code web(local)

        cd project forder
        cd /web
        npm run build

# Connect server via ftp, copy all file in folder disk /home/node_base

## setup vhost nginx

    sudo vi /etc/nginx/sites-enabled/node_base.com.vn
    server {

        listen 80;

        server_name node_base.com.vn;

        root /home/node_base/public;
        location ~* ^.+\.(jpg|jpeg|gif|png|ico|css|zip|tgz|gz|rar|bz2|pdf|txt|tar|wav|bmp|rtf|js|flv|swf|html|htm)$ {
                root   /home/node_base/public;
        }
        location / {

            proxy_pass http://localhost:4000;

            proxy_http_version 1.1;

            proxy_set_header Upgrade $http_upgrade;

            proxy_set_header Connection 'upgrade';

            proxy_set_header Host $host;

            proxy_set_header Access-Control-Allow-Origin *;

            proxy_cache_bypass $http_upgrade;
      }
    }
    sudo vi /etc/nginx/sites-enabled/node_base_web.com.vn
    server {
        listen 80;
        server_name node_base_web.com.vn;
        root /home/node_base_web;
       location / {
        try_files $uri $uri/ /index.html;
      }

    }
    sudo systemctl nginx restart

## Set up ssl

    cat cybers.crt cybes_ca.crt >> bundle.crt
    openssl rsa -in your.key -out /etc/nginx/ssl/node_base.key

    sudo vi /etc/nginx/sites-enabled/node_base.com.vn

    server {
        listen      80;
        server_name  node_base.com.vn;
        return      301 https://$server_name$request_uri;
    }
    server {

        listen      443 ssl http2;
        ssl_certificate     /etc/nginx/ssl/bundle.crt;
        ssl_certificate_key /etc/nginx/ssl/node_base.key;
        server_name node_base.com.vn;

        root /home/node_base;

        location / {

            proxy_pass http://localhost:4000;

            proxy_http_version 1.1;

            proxy_set_header Upgrade $http_upgrade;

            proxy_set_header Connection 'upgrade';

            proxy_set_header Host $host;

            proxy_set_header Access-Control-Allow-Origin *;

            proxy_cache_bypass $http_upgrade;
      }
    }

    sudo vi /etc/nginx/sites-enabled/node_base_web.com.vn
    server {
        listen 80;
        server_name node_base_web.com.vn;
        return      301 https://$server_name$request_uri;
    }
    server {

        listen      443 ssl http2;
        ssl_certificate     /etc/nginx/ssl/bundle.crt;
        ssl_certificate_key /etc/nginx/ssl/node_base_web.key;
        server_name node_base_web.com.vn;
        root /home/vmodev/node_base_web;
       location / {
        try_files $uri $uri/ /index.html;
      }

    }

# For dev run local

+ start project server:

    npm run start.

+ run with nodemon:

    npm run dev.

+ run web: cd /web

    npm run serve
