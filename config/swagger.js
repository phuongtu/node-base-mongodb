import config from 'config';

const swaggerDefinition = {
    info: {
        title: 'Nodejs Base',
        version: '1.0.0',
        description: 'This is a documents api'
    },
    host: `${config.domain}`,
    basePath: '/api/v1',
    securityDefinitions: {
        'Authorize with access token': {
            type: 'apiKey',
            name: 'x-access-token',
            in: 'header'
        }
    }
};

export const options = {
    swaggerDefinition,
    apis: [
        './src/api/auth/auth.router.js',
        './src/api/auth/auth.parameters.yaml',
        './src/api/user/user.parameters.yaml',
        './src/api/user/user.router.js',
        './src/api/user/active/user.active.router.js',
        './src/api/user/password/user.password.router.js',
        './src/api/manage/user/manage.user.router.js',
        './src/api/manage/user/manage.user.parameters.yaml'
    ]
};
