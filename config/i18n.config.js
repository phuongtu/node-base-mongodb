import path from 'path';

import config from 'config';

/** Init multiple languages engine */
// Ref: https://github.com/mashpie/i18n-node

export const I18N_CONFIG = {
    defaultLocale: config.default_locale,
    directory: path.join(__dirname, '../locales'),
    queryParameter: 'lang',
    preserveLegacyCase: true,
    updateFiles: false
};
